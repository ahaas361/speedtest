from socket import error as sockError
from socket import *
import sys, argparse, random, time


alphabetString = "abcdefghijklmnopqrstuvwxzy" #used to randomly generate strings later

#using the argparse module to ensure long options and arguments are correctly parsed
argParser = argparse.ArgumentParser()
argParser.add_argument("--server-ip")
argParser.add_argument("--server-port", required=True, type=int)
arguments = argParser.parse_args()



#if no ip is provided, default to empty string for host
if not arguments.server_ip:
	serverIP = ""

else:
	serverIP = arguments.server_ip #extract ip from command line arguments

serverPort = arguments.server_port #extract port from command line arguments


serverSocket = socket(AF_INET, SOCK_STREAM) #create a TCP socket
serverSocket.bind((serverIP, serverPort)) #assign the given ip and port to this TCP socket
serverSocket.listen(1)


#accept incoming connection
connectionSocket, clientAddress = serverSocket.accept()

while True:		#initial loop that will wait to receive data from client
	

	try:

		data = connectionSocket.recv(10000) #receive data from client, which tests upload speed


		if (len(data) == 3): # "AAA" string from client denoting uploading is done

			for i in range(100): #now send randomized string of length 10,000 100 times, for a total of 1,000,000 bytes sent
				randString = "".join(random.choice(alphabetString) for i in range(10000)) 
				connectionSocket.send(randString)
		
	
	except sockError: #no data in socket, so reloop until there is data input
		continue


	

