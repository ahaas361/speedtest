from socket import *
import sys, argparse, random, time

alphabetString = "abcdefghijklmnopqrstuvwxzy" #used to randomly generate strings later

#using the argparse module to ensure long options and arguments are correctly parsed
argParser = argparse.ArgumentParser()
argParser.add_argument("--server-ip", required=True)
argParser.add_argument("--server-port", required=True, type=int)
arguments = argParser.parse_args()

#extract ip and port from command line arguments
serverIP = arguments.server_ip
serverPort = arguments.server_port

clientSocket = socket(AF_INET, SOCK_STREAM) #create a TCP socket
clientSocket.connect((serverIP, serverPort))





totalBytesSent = 1000000 #sending 1,000,000 MB bytes in 100 intervals of 10,000 bytes each
totalTimeUploading = 0

for i in range(100):
	randString = "".join(random.choice(alphabetString) for i in range(10000)) 
	uploadBeginTime = time.time()
	clientSocket.send(randString)
	uploadEndTime = time.time()
	uploadTimeDelta = uploadEndTime - uploadBeginTime

	totalTimeUploading += uploadTimeDelta

clientSocket.send("AAA") # "AAA" string denotes that uploading is done to the server

totalBytesReceived = 0
totalTimeDownloading = 0

clientSocket.setblocking(0) #don't allow blocking on receiving so we don't count time that the socket is wating to receive data

while True:

	try:
		downloadBeginTime = time.time()
		data = clientSocket.recv(10000)
		downloadEndTime = time.time()
		downloadTimeDelta = downloadEndTime - downloadBeginTime

		totalTimeDownloading += downloadTimeDelta
		totalBytesReceived += len(data)

		if (totalBytesReceived >= 1000000): #all 1,000,000 bytes received from socket
			break

	except error: #error is socket.error
		continue

uploadSpeed = (totalBytesSent / totalTimeUploading) * 8 # 1 byte = 8 bits
downloadSpeed = (totalBytesReceived / totalTimeDownloading) * 8 # 1 byte = 8 bits
print "Upload Speed: %.2f bps" % uploadSpeed
print "Download Speed: %.2f bps" % downloadSpeed
sys.exit(0)